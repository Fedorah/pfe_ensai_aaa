# pfe_ensai_AAA

sources : 
 - https://github.com/yungshenglu/USTC-TK2016
 - https://github.com/yungshenglu/USTC-TFC2016
 - https://github.com/echowei/DeepTraffic

datas : 
 - https://www.unb.ca/cic/datasets/vpn.html

main paper :
 - https://www.mdpi.com/2073-8994/13/6/1080/htm

This branch is set to use the code on windows OS. Enjoy :-)